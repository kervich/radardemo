//
//  AppContext.swift
//  Radar
//
//  Created by Andrey Yurkevich on 18/10/2018.
//  Copyright © 2018 Andrey Yurkevich. All rights reserved.
//

import UIKit

public final class AppContext
{
	public static let shared = AppContext()

	// app constants
	public let bandsCount: Int = 6
	public let objectsCount: Int = 42
	public let bandSpacing: Int = 2

	// Colors
	public let bandColor = #colorLiteral(red: 0, green: 0.2, blue: 1, alpha: 0.0488006162)
	public let bandSpacingColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)

	// images
	public let imageFileName = "plane.tiff"

	private init() {
	}

}

@nonobjc extension UIColor {
	static var band: UIColor {
		return AppContext.shared.bandColor
	}

	static var bandSpacing: UIColor {
		return AppContext.shared.bandSpacingColor
	}
}

extension Int {
	static var bands: Int {
		return AppContext.shared.bandsCount
	}

	static var bogeys: Int {
		return AppContext.shared.objectsCount
	}

	static var bandSpacing: Int {
		return AppContext.shared.bandSpacing
	}

}
