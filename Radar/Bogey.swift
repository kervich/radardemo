//
//  Bogey.swift
//  Radar
//
//  Created by Andrey Yurkevich on 18/10/2018.
//  Copyright © 2018 Andrey Yurkevich. All rights reserved.
//

import UIKit

protocol Drawable {
	func draw(at: CGPoint,
			  radius: CGFloat)
}

struct Bogey
{
	public let bearing: Double
	public let distance: Int

	public init() {
		#if swift(>=4.2)
			self.bearing = Double.random(in: 0..<1)
			self.distance = Int.random(in: 1...Int.bands)
		#else
			self.bearing = drand48()
			self.distance = Int(arc4random_uniform(UInt32(Int.bands)) + 1)
		#endif
	}

}

extension Bogey: Drawable {
	public static var image: UIImage? {
		guard let loadedImage = UIImage(named: AppContext.shared.imageFileName) else {
			assertionFailure("Could not load image")
			return nil
		}

		// render the image in a GC sans alpha channel
		let imageSize = loadedImage.size
		UIGraphicsBeginImageContextWithOptions(imageSize, true, 0)
		loadedImage.draw(in: CGRect(origin: CGPoint.zero, size: imageSize))

		guard let gcImage = UIGraphicsGetImageFromCurrentImageContext() else {
			assertionFailure("Cannot create UIImage from GC")
			return loadedImage
		}
		UIGraphicsEndImageContext()

		// Mask green as transparent and yield the result as UIImage.
		// The color mask is a bit off due to faulty image used in this demo, sorry for that
		let colorMask: [CGFloat] = [0, 16, 240, 255, 0, 16]
		guard let maskedCGImage = gcImage.cgImage?.copy(maskingColorComponents: colorMask) else {
			assertionFailure("Could not map transparency")
			return gcImage
		}

		return UIImage(cgImage: maskedCGImage)
	}

	func draw(at point: CGPoint, radius: CGFloat) {
		// Calculate CGRect that would fit inside a circle with given radius r.
		// That gives each side is r*sqrt(2), but in the perfect world we may also want to rotate the bogey image to align it respective to its bearing angle.
		let side = radius * sqrt(2)
		let rect = CGRect(x: point.x - side/2, y: point.y - side/2, width: side, height: side)

		// Again, in perfect world we'd also CGAffineTransform() the image to rotate it for better visuals
		Bogey.image?.draw(in: rect)
	}

}
