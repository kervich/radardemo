//
//  RadarView.swift
//  Radar
//
//  Created by Andrey Yurkevich on 18/10/2018.
//  Copyright © 2018 Andrey Yurkevich. All rights reserved.
//

import UIKit

final class RadarView: UIView
{
	private var radius: CGFloat {
		return min(self.bounds.width, self.bounds.height) / 2
	}

	func addCircle(_ step: Int) {
		let bandRadius = CGFloat(Int(self.radius) / Int.bands * step)
		let boundingBox = CGRect(x: self.center.x - bandRadius,
								 y: self.center.y - bandRadius,
								 width: bandRadius * 2,
								 height: bandRadius * 2)

		let path = UIBezierPath(ovalIn: boundingBox)

		UIColor.band.setFill()
		path.fill()

		UIColor.bandSpacing.setStroke()
		path.lineWidth = CGFloat(Int.bandSpacing)
		path.stroke()
	}

	func addBogey(_ aBogey: Bogey) {
		let bandWidth = self.radius / CGFloat(Int.bands)
		let r = CGFloat(aBogey.distance) * bandWidth - bandWidth/2 - CGFloat(Int.bandSpacing)
		let a = CGFloat(2*Double.pi * aBogey.bearing)
		let pointX = r*cos(a) + self.center.x
		let pointY = r*sin(a) + self.center.y

		// reduce bogey radius by spacing width for extra aesthetics
		aBogey.draw(at: CGPoint(x: pointX, y: pointY), radius: bandWidth/2 - CGFloat(Int.bandSpacing))
	}

	override func draw(_ rect: CGRect) {
		for step in 1...Int.bands {
			self.addCircle(step)
		}

		for _ in 1...Int.bogeys {
			self.addBogey(Bogey())
		}
	}

	override init(frame: CGRect) {
		super.init(frame: frame)
		self.backgroundColor = UIColor.white
	}

	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
	}
	
}
