//
//  ViewController.swift
//  Radar
//
//  Created by Andrey Yurkevich on 18/10/2018.
//  Copyright © 2018 Andrey Yurkevich. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		let radarView = RadarView(frame: self.view.frame)
		self.view.addSubview(radarView)
	}

}

